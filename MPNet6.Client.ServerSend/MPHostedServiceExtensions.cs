﻿using Microsoft.Extensions.DependencyInjection;
namespace MPNet6.Client.ServerSend;
public static class MPHostedServiceExtensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddMPClient(this IServiceCollection services)
    {
        return services.AddHostedService(serviceProvider =>
        {
            return new MPHostedService(serviceProvider.GetRequiredService<ILoggerFactory>().CreateLogger("MPClientSend"));
        });

    }
}


