﻿global using Microsoft.Extensions.Logging;

global using System.Diagnostics;

using Microsoft.Extensions.Hosting;

namespace MPNet6.Client.ServerSend;
public class MPHostedService : IHostedService
{
    private readonly ILogger _Logger;

    public MPHostedService(ILogger logger)
    {
        _Logger = logger;
    }
    public Task StartAsync(CancellationToken cancellationToken)
    {
        MPClientSend.Start(_Logger);
        _Logger.Log(LogLevel.Information, $"MPClient started.");
        return Task.CompletedTask;
    }


    public Task StopAsync(CancellationToken cancellationToken)
    {
        _Logger.Log(LogLevel.Information, $"MPClient stopping...");
        Stopwatch sw = Stopwatch.StartNew();
        MPClientSend.Stop();
        _Logger.Log(LogLevel.Information, $"MPClientstopped in {sw.ElapsedMilliseconds}ms.");
        return Task.CompletedTask;
    }
}

