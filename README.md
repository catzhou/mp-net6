# MpNet6

#### 介绍
ASP.Net6.0消息推送


1. MPNet6.Server:消息推送的服务端，在ASP.NET程序使用。除了管理客户端意外，还可以直接发送消息给指定ClientId；
1. MPNet6.Client.Receive:消息推送的接收客户端，一般在WinForm或者Wpf程序使用；
1. MPNet6.Client.Send:消息推送的发送客户端，一般在WinForm或者Wpf程序使用；
1. MPNet6.Client.ServerSend:消息推送的发送客户端，在ASP.NET程序使用。

 **MPNet.Client已经丢弃，不再升级。** 