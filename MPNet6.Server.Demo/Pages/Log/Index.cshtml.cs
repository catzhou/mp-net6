﻿using LogNet6.SqlServer;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MPNet6.Server.Demo.Pages.Log;

public class IndexModel : PageModel
{
    [BindProperty(SupportsGet = true)]
    public int PageNo { get; set; } = 0;
    [BindProperty(SupportsGet = true)]
    public int PageSize { get; set; } = 50;

    public List<SqlserverLoggerItem> Items { get; set; } = default!;
    public void OnGet([FromServices] ISqlServerLoggerProcessor service)
    {
        Items = service.GetLogs(PageNo, PageSize);
    }
}
