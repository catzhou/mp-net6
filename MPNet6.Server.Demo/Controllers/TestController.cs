﻿using Microsoft.AspNetCore.Mvc;

using MPNet6.Client.ServerSend;

using System.Diagnostics;

namespace MPNet6.Server.Demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        static int _Index = 0;
        [HttpGet]
        public IActionResult SendUserMPServer(string clientIds = "1", int times = 10)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var ids = clientIds.Split(',');
            for (int ii = 0; ii < times; ii++)
            {
                foreach (var id in ids)
                {
                    MPServer.SendMessage(id, $"ClientId:{id};Index:{++_Index}___hello");
                }
            }
            return Ok(sw.ElapsedMilliseconds.ToString() + "ms " + _Index);
        }
        [HttpGet("mpclient")]
        public IActionResult SendUserMPClient(string clientIds = "1", int times = 10)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var ids = clientIds.Split(',');
            for (int ii = 0; ii < times; ii++)
            {
                foreach (var id in ids)
                {
                    MPClientSend.Send("127.0.0.1", 12888, id, $"ClientId:{id};Index:{++_Index}___hello");
                }
            }
            return Ok(sw.ElapsedMilliseconds.ToString() + "ms " + _Index);
        }
    }
}
