global using MPNet6.Server;

global using System.Text.Json.Serialization;

using LogNet6;
using LogNet6.SqlServer;

using MPNet6.Client.ServerSend;
using MPNet6.Server.Demo.Models;

using System.Text.Encodings.Web;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("LogConnection");
builder.Services.AddSqlServerLogger(connectionString, "MessagePush");
builder.Logging.AddNet6LoggerFilter(builder.Configuration.GetSection("Logging:LogLevel"));
builder.Services.AddMPServer(12888, () => new ClientMessageProcessor().GetMessages(connectionString), msgs => new ClientMessageProcessor().SaveMessages(connectionString, msgs));
builder.Services.AddMPClient();


builder.Services.AddRazorPages();
builder.Services.AddControllers().AddXmlSerializerFormatters().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
    options.JsonSerializerOptions.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
});

builder.Services.AddSwaggerGen();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
    //c.SwaggerEndpoint("/swagger/v2/swagger.json", "V2 Docs");
    c.DefaultModelsExpandDepth(-1);
});

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllers();
app.MapRazorPages();
app.Run();