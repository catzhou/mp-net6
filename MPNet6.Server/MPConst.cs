﻿namespace MPNet6.Server;

internal class MPConsts
{
    public const byte ConnectSuccessMore = 0;
    public const byte ReconnectSuccessMore = 1;
    public const byte ConnectSuccessOnce = 2;
    public const byte ReconnectSuccessOnce = 3;
    public const char Failure = (char)100;
    public const byte Echo = 200;
}
