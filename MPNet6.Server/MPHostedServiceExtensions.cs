﻿using Microsoft.Extensions.DependencyInjection;

namespace MPNet6.Server;

public static class MPHostedServiceExtensions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <param name="port">服务器侦听的端口号</param>
    /// <param name="getClientMessages">获取上次系统退出时保存的未推送的消息列表</param>
    /// <param name="SaveClientMessages">保存系统退出时未推送的消息列表</param>
    /// <returns></returns>
    public static IServiceCollection AddMPServer(this IServiceCollection services, int port = 12347, Func<List<MPClientMessage>>? getClientMessages = null, Action<List<MPClientMessage>>? SaveClientMessages = null)
    {
        return services.AddHostedService(serviceProvider =>
        {
            return new MPHostedService(port, getClientMessages, SaveClientMessages, serviceProvider.GetRequiredService<ILoggerFactory>().CreateLogger("MPServer"));
        });

    }
}


