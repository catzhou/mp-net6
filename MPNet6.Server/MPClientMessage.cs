﻿using System.Text.Json.Serialization;

namespace MPNet6.Server;
public class MPClientMessage
{
    [JsonPropertyName("C")]
    public string ClientId { get; set; } = default!;
    [JsonPropertyName("M")]
    public string Msgs { get; set; } = default!;
    public MPClientMessage()
    { }
    public MPClientMessage(string clientId, string msgs)
    {
        ClientId = clientId;
        Msgs = msgs;
    }
}

