using MPNet6.Client.Send;
using MPNet6.Client.Receive;

namespace MPNet6.Client.Demo;

public partial class Form1 : Form
{
    private MPClientReceive? _MPClient;
    public Form1()
    {
        InitializeComponent();
        this.tbClientId.Text = "1";
        this.tbHost.Text = "127.0.0.1";
        this.tbPort.Text = "12888";
        this.tbSecondsDelay.Text = "0";
        this.tbMsg.Text = "Hello";
    }

    private void button1_Click(object sender, EventArgs e)
    {
        _MPClient?.Stop();
        _MPClient = new(this.tbHost.Text, int.Parse(this.tbPort.Text), this.tbClientId.Text, int.Parse(this.tbSecondsDelay.Text));
        _MPClient.EventMP += _MPClient_EventMP;
        _MPClient.Start();
    }

    private void _MPClient_EventMP(MpArgs obj)
    {
        this.listBox1.Invoke(() => this.listBox1.Items.Insert(0, DateTime.Now.ToString("mm:ss:fff") + "------" + obj.ToString()));
    }

    private void button2_Click(object sender, EventArgs e)
    {
        _MPClient?.Stop();
    }

    private void button3_Click(object sender, EventArgs e)
    {
        MPClientSend.Send(this.tbHost.Text, int.Parse(this.tbPort.Text), this.tbClientId.Text, this.tbMsg.Text + " " + DateTime.Now.ToString("mm:ss:fff"));
    }
}